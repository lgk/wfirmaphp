<?php
namespace wFirmaPHP;

use GuzzleHttp;

class wFirma extends GuzzleHttp\Client {

    const BASE_URL = 'http://api2.wfirma.pl';

    public function __construct($username, $password)
    {
        parent::__construct([
            'base_url' => self::BASE_URL,
            'defaults' => [
                'auth' => [$username, $password]
            ]
        ]);
    }

    /**
     *
     * @param Invoice $invoice
     * @return \GuzzleHttp\Message\ResponseInterface
     */
    public function createInvoice(Invoice $invoice)
    {
        return $this->post('/invoices/add?inputFormat=json&outputFormat=xml', [
            'body' => (string) $invoice
        ]);
    }

    /**
     *
     * @param numeric $id
     * @return \GuzzleHttp\Message\ResponseInterface
     */
    public function removeInvoice($id)
    {
        return $this->get('/invoices/remove/'.$id);
    }

    /**
     *
     * @param numeric $id
     * @return \GuzzleHttp\Message\ResponseInterface
     */
    public function downloadInvoice($id)
    {
        return $this->get('/invoices/download/'.$id);
    }

    /**
     *
     * @param numeric $id
     * @return \GuzzleHttp\Message\ResponseInterface
     */
    public function getInvoice($id)
    {
        return $this->get('/invoices/get/'.$id);
    }
}
