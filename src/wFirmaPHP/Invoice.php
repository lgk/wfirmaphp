<?php
namespace wFirmaPHP;

use wFirmaPHP\Invoice\Item;

use JsonSerializable;

class Invoice implements JsonSerializable {

    private $struct = [];

    public function __construct()
    {
        $this->struct['invoices']['invoice']['price_type'] = 'netto';
        //$this->struct['invoices']['invoice']['contractor'] = [];
        $this->struct['invoices']['invoice']['paymentmethod'] = 'transfer';
        $this->struct['invoices']['invoice']['disposaldate_format'] = 'day';
        $this->struct['invoices']['invoice']['disposaldate'] = date('Y-m-d');
        $this->struct['invoices']['invoice']['invoicecontents'] = [];
    }

    public function setContractor($name, $nip, $street, $zip, $city, $country)
    {
        $this->struct['invoices']['invoice']['contractor']['name'] = $name;
        $this->struct['invoices']['invoice']['contractor']['nip'] = $nip;
        $this->struct['invoices']['invoice']['contractor']['street'] = $street;
        $this->struct['invoices']['invoice']['contractor']['zip'] = $zip;
        $this->struct['invoices']['invoice']['contractor']['city'] = $city;
        $this->struct['invoices']['invoice']['contractor']['country'] = $country;
        return $this;
    }

    public function setValue($name, $value)
    {
        $this->struct['invoices']['invoice'][$name] = $value;
        return $this;
    }

    public function addItem(Item $item)
    {
        $this->struct['invoices']['invoice']['invoicecontents'][]['invoicecontent']= $item->toArray();
        return $this;
    }

    public function toArray()
    {
        return $this->struct;
    }

    public function jsonSerialize()
    {
        return json_encode($this->toArray());
    }

    public function __toString()
    {
        return $this->jsonSerialize();
    }
}
