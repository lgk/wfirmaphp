<?php
namespace wFirmaPHP\Invoice;

class Item {

    private $name;
    private $unit;
    private $count;
    private $price;
    private $vat;
    private $discount;
    private $discount_percent;

    public function __construct($name, $unit, $count, $price, $vat)
    {
        $this->name = $name;
        $this->unit = $unit;
        $this->count = $count;
        $this->price = $price;
        $this->vat = $vat;
        $this->discount = 0;
        $this->discount_percent = 0;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getUnit()
    {
        return $this->unit;
    }

    public function setUnit($unit)
    {
        $this->unit = $unit;
        return $this;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function setCount($count)
    {
        $this->count = $count;
        return $this;
    }

    public function getPrice()
    {
        return $this->price;
    }

    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    public function getVat()
    {
        return $this->vat;
    }

    public function setVat($vat)
    {
        $this->vat = $vat;
        return $this;
    }

    public function setDiscount($discount)
    {
        $this->discount = (int) $discount > 0;
        $this->discount_percent = $discount > 0 ? $discount : 0;
        return $this;
    }

    public function getDiscount()
    {
        return $this->discount;
    }

    public function toArray()
    {
        return get_object_vars($this);
    }
}
