<?php
namespace wFirmaPHP\Invoice;

final class Type {
    const VAT = 'normal';
    const PROFORMA = 'proforma';
    const RECEIPT_NORMAL = 'receipt_normal';
    const RECEIPT_FISCAL = 'receipt_fiscal_normal';
}
